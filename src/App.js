import React from 'react'
import TarjetaFruta from './componentes/TarjetaFruta/'

const App = () => (
    <div>
        <TarjetaFruta name={"Sandia"} price={2.00}></TarjetaFruta>
        <TarjetaFruta name={"Pera"} price={3.50}></TarjetaFruta>
    </div>
)

export default App